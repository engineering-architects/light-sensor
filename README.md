## LIGHT SENSOR PROJECT
A repo that contains the all documentation and files of our Light sensor projet. These files include plans and schematics of our HAT PCB, Firmware, Production docs and Simulations. The vision for this light sensor project is to build an HAT that controls the light in buildings based on the amount of light coming inside the room. This is one of the initiatives designed to save power in a country going through a power crisis.

## How to use repo
1. Navigate around the repo to see user documentation or our product, look at the schematics and photos, simulations. 

2. We have also included the budget and materials, with a detailed production process. 

3. Not all the data is readily available as this is an on going project, so pull down updates regularly to see what is new.


# Istallations
All the installations you will need will be posted here as time goes.

## Usage
Description, scenarios and requirements of our products have been uploaded [here](https://gitlab.com/engineering-architects/light-sensor/-/tree/main/Docs).
This also includes the block diagram of our HAT.

## Support
For support and help you can contact the creator of the repo, Kirsten Maluleke at nyiko.kirstenm@gmail.com

## License
[Creative Commons Attribution 4.0 International license](https://choosealicense.com/licenses/cc-by-4.0/)
